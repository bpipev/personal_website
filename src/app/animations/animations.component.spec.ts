import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AnimationsComponent } from './animations.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('AnimationsComponent', () => {
    let component: AnimationsComponent;
    let fixture: ComponentFixture<AnimationsComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [AnimationsComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AnimationsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
