export interface IAppConfig {
    env: {
        name: string;
    };
    fb_app_id: string;
    apis: {
        playground: {
            domain: string;
            paths: {
                login: string;
                plans: string;
                steps: string;
                logout: string;
                public: string;
                publicPlan: string;
                makePlanPublic: string;
                makePlanPrivate: string;
                schedules: string;
                schedulesPeople: string;
                scheduledPerson: string;
                workPreference: string;
            };
        };
    };
}
