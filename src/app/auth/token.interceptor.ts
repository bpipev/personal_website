import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private us: UserService) {}
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        const token = this.us.getToken();
        if (token != null) {
            const bearerToken = `Bearer ${token}`;
            request = request.clone({
                headers: request.headers.append('Authorization', bearerToken)
            });
        }

        return next.handle(request);
    }
}
