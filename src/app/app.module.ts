import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { MessagesComponent } from './messages/messages.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WeatherComponent } from './weather/weather.component';
import { StocksComponent } from './stocks/stocks.component';

import { CdkTableModule } from '@angular/cdk/table';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatNativeDateModule } from '@angular/material/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDividerModule } from '@angular/material/divider';
import { LicensesComponent } from './licenses/licenses.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AnimationsComponent } from './animations/animations.component';
import { FancyscrollComponent } from './fancyscroll/fancyscroll.component';
import { GoogleLoginComponent } from './login/google-login/google-login.component';
import { AppConfigService } from './app-config.service';
import { PlanComponent } from './plan/plan.component';
import { AddPlanComponent } from './plan/add-plan/add-plan.component';
import { ViewPlanComponent } from './plan/view-plan/view-plan.component';
import { PlanStepsComponent } from './plan-steps/plan-steps.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AddStepComponent } from './plan-steps/add-step/add-step.component';
import { httpInterceptorProviders } from './http-interceptors';
import { LoginComponent } from './login/login.component';
import { FacebookLoginComponent } from './login/facebook-login/facebook-login.component';
import { ButtonLoginComponent } from './login/button-login/button-login.component';
import { SettingsComponent } from './settings/settings.component';
import { ViewPlanDashboardComponent } from './plan/view-plan-dashboard/view-plan-dashboard.component';
import { StepComponent } from './plan-steps/step/step.component';
import { StepValidationDirective } from './plan-steps/validation/step-validation.directive';
import { MapComponent } from './map/map.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { AddPersonComponent } from './schedule/add-person/add-person.component';
import { CalendarViewComponent } from './schedule/calendar-view/calendar-view.component';
import { AddScheduleComponent } from './schedule/add-schedule/add-schedule.component';
import { ScheduleListComponent } from './schedule/schedule-list/schedule-list.component';
import { RemovePersonComponent } from './schedule/remove-person/remove-person.component';
import { ViewPersonComponent } from './schedule/view-person/view-person.component';
import { AiComponent } from './ai/ai.component';
import { NandComponent } from './ai/nand/nand.component';
import { NetworkComponent } from './ai/network/network.component';
import { ImageComponent } from './ai/image/image.component';

export function initializeApp(appConfig: AppConfigService) {
    return () => appConfig.load();
}

@NgModule({
    declarations: [
        AppComponent,
        MessagesComponent,
        DashboardComponent,
        WeatherComponent,
        StocksComponent,
        LicensesComponent,
        AnimationsComponent,
        FancyscrollComponent,
        GoogleLoginComponent,
        PlanComponent,
        AddPlanComponent,
        ViewPlanComponent,
        PlanStepsComponent,
        PageNotFoundComponent,
        AddStepComponent,
        LoginComponent,
        FacebookLoginComponent,
        ButtonLoginComponent,
        SettingsComponent,
        ViewPlanDashboardComponent,
        StepComponent,
        StepValidationDirective,
        MapComponent,
        ScheduleComponent,
        AddPersonComponent,
        CalendarViewComponent,
        AddScheduleComponent,
        ScheduleListComponent,
        RemovePersonComponent,
        ViewPersonComponent,
        AiComponent,
        NandComponent,
        NetworkComponent,
        ImageComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        CdkTableModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatStepperModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        BrowserAnimationsModule,
        ServiceWorkerModule.register('/ngsw-worker.js', {
            enabled: environment.production
        })
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: initializeApp,
            deps: [AppConfigService],
            multi: true
        },
        httpInterceptorProviders
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
