import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddPlanComponent } from './add-plan.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PlanService } from '../../plan.service';
import { Plan } from '../../plan';
import { Observable } from 'rxjs';

describe('AddPlanComponent', () => {
    let component: AddPlanComponent;
    let fixture: ComponentFixture<AddPlanComponent>;
    let planServiceStub: Partial<PlanService>;

    planServiceStub = {
        addPlan(plan: Plan, isSignedIn: boolean): Observable<Plan> {
            return Observable.create();
        }
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [ReactiveFormsModule, FormsModule],
            declarations: [AddPlanComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: PlanService,
                    useValue: planServiceStub
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddPlanComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
