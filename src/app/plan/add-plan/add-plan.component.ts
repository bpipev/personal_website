import { Component, OnInit } from '@angular/core';
import { Plan } from '../../plan';
import { NgForm } from '@angular/forms';
import { PlanService } from '../../services/plan.service';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'app-add-plan',
    templateUrl: './add-plan.component.html',
    styleUrls: ['./add-plan.component.css']
})
export class AddPlanComponent implements OnInit {
    plan = new Plan('', '');
    constructor(
        private planService: PlanService,
        private userService: UserService
    ) {}

    ngOnInit() {}

    onSubmit(planForm: NgForm) {
        if (planForm.valid) {
            this.planService
                .addPlan(this.plan, this.userService.isSignedIn)
                .subscribe(() => {
                    planForm.resetForm();
                });
        }
    }
}
