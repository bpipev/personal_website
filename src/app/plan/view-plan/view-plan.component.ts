import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { PlanService } from '../../services/plan.service';
import { Plan } from '../../plan';
import { Router } from '@angular/router';
import { Subject, Observable, PartialObserver } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'app-view-plan',
    templateUrl: './view-plan.component.html',
    styleUrls: ['./view-plan.component.css']
})
export class ViewPlanComponent implements OnInit, OnDestroy {
    private onDestroy$ = new Subject();
    constructor(
        private planService: PlanService,
        private us: UserService,
        private router: Router
    ) {}

    ngOnInit() {
        // https://medium.com/thecodecampus-knowledge/the-easiest-way-to-unsubscribe-from-observables-in-angular-5abde80a5ae3
        this.us.userUpdated$
            .pipe(takeUntil(this.onDestroy$))
            .subscribe(() => this.planService.getPlanList(this.us.isSignedIn));
    }

    get planChanged$(): Observable<Plan[]> {
        return this.planService.planChanged$;
    }

    get isLoading$(): Observable<Boolean> {
        return this.planService.isLoading$;
    }

    gotoPlan(plan: Plan) {
        this.router.navigate(['/plan', plan.plan_id], {
            queryParams: { isLocal: plan.isLocal }
        });
    }

    uploadPlan(plan: Plan) {
        this.planService.addPlan(plan, this.us.isSignedIn).subscribe(x => {
            this.planService.deletePlan(plan, this.us.isSignedIn).subscribe();
        });
    }

    deletePlan(plan: Plan) {
        this.planService.deletePlan(plan, this.us.isSignedIn).subscribe();
    }

    makePlanPublic(plan: Plan) {
        this.planService.makePlanPublic(plan.plan_id).subscribe(status => {
            plan.is_public = status.success ? true : plan.is_public;
        });
    }

    makePlanPrivate(plan: Plan) {
        this.planService.makePlanPrivate(plan.plan_id).subscribe(status => {
            plan.is_public = status.success ? false : plan.is_public;
        });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next(1);
    }
}
