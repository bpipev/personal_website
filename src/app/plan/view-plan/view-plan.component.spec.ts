import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ViewPlanComponent } from './view-plan.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppConfigService } from '../../app-config.service';

describe('ViewPlanComponent', () => {
    let component: ViewPlanComponent;
    let fixture: ComponentFixture<ViewPlanComponent>;

    beforeEach(waitForAsync(() => {
        AppConfigService.settings = {
            env: {
                name: 'string'
            },
            fb_app_id: 'string',
            apis: {
                playground: {
                    domain: 'string',
                    paths: {
                        login: 'string',
                        plans: 'string',
                        steps: 'string',
                        logout: 'string',
                        public: 'string',
                        publicPlan: 'string'
                    }
                }
            }
        };
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterTestingModule],
            declarations: [ViewPlanComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ViewPlanComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
