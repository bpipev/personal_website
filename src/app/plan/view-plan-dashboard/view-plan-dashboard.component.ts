import { Component, OnInit } from '@angular/core';
import { Plan } from '../../plan';
import { Router } from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';

@Component({
    selector: 'app-view-plan-dashboard',
    templateUrl: './view-plan-dashboard.component.html',
    styleUrls: ['./view-plan-dashboard.component.css']
})
export class ViewPlanDashboardComponent implements OnInit {
    plans: Plan[] = new Array<Plan>();
    constructor(private dashboardService: DashboardService, private router: Router) {}

    ngOnInit() {
        this.dashboardService.getDashboard().subscribe(dashboard => {
            this.plans = dashboard.plans;
        });
    }

    gotoPlan(plan: Plan) {
        this.router.navigate(['/plan', plan.plan_id], {
            queryParams: { isLocal: plan.isLocal, isPublic: true }
        });
    }
}
