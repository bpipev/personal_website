import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ViewPlanDashboardComponent } from './view-plan-dashboard.component';

describe('ViewPlanDashboardComponent', () => {
  let component: ViewPlanDashboardComponent;
  let fixture: ComponentFixture<ViewPlanDashboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPlanDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPlanDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
