import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PlanComponent } from './plan.component';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@Component({ selector: 'app-view-plan', template: '' })
class AppViewPlanStubComponent {}

@Component({ selector: 'app-add-plan', template: '' })
class AppAddPlanStubComponent {}

describe('PlanComponent', () => {
    let component: PlanComponent;
    let fixture: ComponentFixture<PlanComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                PlanComponent,
                AppViewPlanStubComponent,
                AppAddPlanStubComponent
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PlanComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
