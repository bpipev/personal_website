import {
    Component,
    OnInit,
    OnDestroy
} from '@angular/core';
import { UserService, LOGIN_TYPE } from '../../services/user.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { GoogleLoginService } from '../../services/google-login.service';
import { FacebookLoginService } from '../../services/facebook-login.service';
import { User } from '../../user';

@Component({
    selector: 'app-button-login',
    templateUrl: './button-login.component.html',
    styleUrls: ['./button-login.component.css']
})
export class ButtonLoginComponent implements OnInit, OnDestroy {
    loginText: string;
    isLoading: boolean;
    private onDestroy$ = new Subject();

    constructor(
        private userService: UserService,
        private router: Router,
        private gls: GoogleLoginService,
        private fls: FacebookLoginService
    ) {}

    ngOnInit() {
        this.isLoading = true;
        (window as any).fbAsyncInit = this.fls.onFbAsyncInit;
        this.fls.initSignin();
        this.gls.initSignIn();
        this.userService.initialize();
        this.userService.userUpdated$
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((user: User) => {
                if (user.isSignedIn) {
                    this.loginText = $localize`:@@signout:Sign Out`; // TODO: add name
                } else {
                    this.loginText = $localize`:@@login:Login`;
                }
                this.isLoading = false;
            });
    }

    loginOut() {
        if (this.userService.isSignedIn) {
            if (this.userService.loginType === LOGIN_TYPE.GOOGLE) {
                this.gls.signOut();
            } else if (this.userService.loginType === LOGIN_TYPE.FACEBOOK) {
                this.fls.signOut();
            }
            this.router.navigate(['/dashboard']);
        } else {
            this.router.navigate(['/login']);
        }
    }

    ngOnDestroy() {
        this.onDestroy$.next(1);
    }
}
