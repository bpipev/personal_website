import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FacebookLoginService } from '../../services/facebook-login.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-facebook-login',
    templateUrl: './facebook-login.component.html',
    styleUrls: ['./facebook-login.component.css']
})
export class FacebookLoginComponent implements OnInit, OnDestroy {
    private onDestroy$ = new Subject();
    isLoading: boolean;
    constructor(
        private router: Router,
        private facebookLogin: FacebookLoginService,
        private changeDetector: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.isLoading = true; // TODO: Night mode?
        this.facebookLogin.userChanged$
            .pipe(takeUntil(this.onDestroy$))
            .subscribe(statusResponse => {
                this.isLoading = false;
                if (statusResponse.status === 'connected') {
                    this.router.navigate(['/dashboard']);
                }
            });
        this.facebookLogin.hasInitialized$
            .pipe(takeUntil(this.onDestroy$))
            .subscribe(() => {
                this.isLoading = false;
                this.changeDetector.detectChanges();
                this.facebookLogin.parse();
            });
    }

    ngOnDestroy() {
        this.onDestroy$.next(1);
    }
}
