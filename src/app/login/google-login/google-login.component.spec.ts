import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GoogleLoginComponent } from './google-login.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GoogleLoginService } from '../../google-login.service';
import { Observable } from 'rxjs';

describe('GoogleLoginComponent', () => {
    let component: GoogleLoginComponent;
    let fixture: ComponentFixture<GoogleLoginComponent>;
    let googleLoginServiceStub: Partial<GoogleLoginService>;

    googleLoginServiceStub = {
        initSignIn() {},
        userChanged$: Observable.create()
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            declarations: [GoogleLoginComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: GoogleLoginService,
                    useValue: googleLoginServiceStub
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GoogleLoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
