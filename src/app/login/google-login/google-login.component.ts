import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { PlaygroundService } from '../../services/playground.service';
import { GoogleLoginService } from '../../services/google-login.service';
import { UserService, LOGIN_TYPE } from '../../services/user.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    selector: 'app-google-login',
    templateUrl: './google-login.component.html',
    styleUrls: ['./google-login.component.css']
})
export class GoogleLoginComponent implements OnInit, OnDestroy {
    private onDestroy$ = new Subject();
    isLoggedIn = false;
    userName: string;
    isLoading = true;

    constructor(
        private cd: ChangeDetectorRef,
        private ps: PlaygroundService,
        private gls: GoogleLoginService,
        private us: UserService,
        private router: Router
    ) {}

    failure() {
        console.log('failure');
    }

    ngOnInit() {
        this.gls.userChanged$
            .pipe(takeUntil(this.onDestroy$))
            .subscribe(user => {
                this.userChanged(user);
            });
    }

    userChanged(user: gapi.auth2.GoogleUser) {
        this.isLoggedIn = user.isSignedIn();
        this.isLoading = false;
        if (this.isLoggedIn) { // TODO: remove?
            const user_id = user.getAuthResponse().id_token;
            this.ps.googleLogin(user_id).subscribe(response => {
                this.us.userUpdated(response.token, LOGIN_TYPE.GOOGLE);
            });
            this.userName = user.getBasicProfile().getName();
            this.router.navigate(['/dashboard']); // TODO: navigate to previous or dashboard
        } else {
            this.renderGoogleSignIn();
        }
        console.log(`userChanged signedIn: ${this.isLoggedIn}`);
    }

    private renderGoogleSignIn() {
        this.cd.detectChanges();
        gapi.signin2.render('signin', {
            scope: 'profile email',
            width: 254,
            height: 40,
            longtitle: true,
            theme: 'light',
            onfailure: () => {
                this.failure();
            }
        });
    }

    ngOnDestroy() {
        this.onDestroy$.next(1);
    }
}
