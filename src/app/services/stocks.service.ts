import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageService } from './message.service';
import { Observable, of, timer } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';
import { Stocks, GlobalQuoteResult } from './../stocks';

export enum TimeSeriesFunction {
    TIME_SERIES_INTRADAY,
    TIME_SERIES_DAILY,
    TIME_SERIES_DAILY_ADJUSTED,
    TIME_SERIES_WEEKLY,
    TIME_SERIES_WEEKLY_ADJUSTED,
    TIME_SERIES_MONTHLY,
    TIME_SERIES_MONTHLY_ADJUSTED,
    GLOBAL_QUOTE
}

@Injectable({
    providedIn: 'root'
})
export class StocksService {
    private useCache = false;
    private getGlobalQuoteCache: {[symbol: string]: GlobalQuoteResult } = {};
    private stocksUrl = 'https://www.alphavantage.co/query';
    // private stocksApi = 'PSH760D9HP1JB2L0';
    private stocksApi = 'YCZYY2X5V39CTW18';

    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) { }

    /** Log a StocksService message with the messageService */
    private log(message: string) {
        this.messageService.add('StocksService: ' + message);
    }

    getStocksTimeSeriesDaily(symbol: string): Observable<any> {
        const url = `${this.stocksUrl}?function=${
            TimeSeriesFunction[TimeSeriesFunction.TIME_SERIES_DAILY]
        }&symbol=${symbol}&apikey=${this.stocksApi}&datatype=csv`;

        return this.http.get(url, { responseType: 'text' }).pipe(
            map(x => Stocks.fromText(x, symbol)),
            tap(_ => this.log(`Stocks queried for symbol: ${symbol}`)),
            catchError(this.handleError('getStocksTimeSeriesData'))
        );
    }

    getGlobalQuote(symbol: string): Observable<any> {
        const url = `${this.stocksUrl}?function=${
            TimeSeriesFunction[TimeSeriesFunction.GLOBAL_QUOTE]
        }&symbol=${symbol}&apikey=${this.stocksApi}&datatype=json`;

        if (this.useCache) {
            this.log(`Using stock cache for symbol: ${symbol}`);
            return of(this.getGlobalQuoteCache[symbol]);
        } else {
            return this.http.get(url, { responseType: 'text' }).pipe(
                map(x => GlobalQuoteResult.fromJsonText(x)),
                tap(results => {
                    this.getGlobalQuoteCache[symbol] = results;
                    this.useCache = true;
                    const twoMinutes = 2 * 60 * 1000;
                    timer(twoMinutes).subscribe(_ => (this.useCache = false));
                    this.log(`Stocks queried for symbol: ${symbol}`);
                }),
                catchError(this.handleError('getGlobalQuote'))
            );
        }
    }

    getStocksTimeSeriesIntraDay(
        symbol: string,
        interval: number
    ): Observable<any> {
        const url = `${this.stocksUrl}?function=${
            TimeSeriesFunction[TimeSeriesFunction.TIME_SERIES_INTRADAY]
        }&symbol=${symbol}&interval=${interval}&apikey=${
            this.stocksApi
        }&datatype=csv`;

        return this.http.get(url, { responseType: 'text' }).pipe(
            map(x => Stocks.fromText(x, symbol)),
            tap(_ => this.log(`Stocks queried for symbol: ${symbol}`)),
            catchError(this.handleError('getStocksTimeSeriesData'))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
