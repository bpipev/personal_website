import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from './message.service';
import { Observable ,  of } from 'rxjs';
import { Weather } from '../weather';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class WeatherService {
    private weatherServiceUrl = 'https://api.openweathermap.org/data/2.5/weather';
    private weatherApiKey = '0e52700e41b12db6f305bb80009f51ba';
    private units = 'metric';

    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) {}

    getWeatherByCity(cityName: string): Observable<Weather> {
        const url = `${this.weatherServiceUrl}?q=${cityName}&appid=${
            this.weatherApiKey
        }&units=${this.units}`;
        return this.http
            .get<Weather>(url)
            .pipe(
                tap(_ => this.log(`Weather queried for city=${cityName}`)),
                catchError(this.handleError<Weather>('getWeatherByCity'))
            );
    }

    getWeatherByLocation(position: GeolocationPosition) {
        const url = `${this.weatherServiceUrl}?lat=${
            position.coords.latitude
        }&lon=${position.coords.longitude}&appid=${this.weatherApiKey}&units=${
            this.units
        }`;
        return this.http
            .get<Weather>(url)
            .pipe(
                tap(_ =>
                    this.log(
                        `Weather queried for lat=${
                            position.coords.latitude
                        } lon=${position.coords.longitude}`
                    )
                ),
                catchError(this.handleError<Weather>('getWeatherByLocation'))
            );
    }

    /** Log a WeatherService message with the messageService */
    private log(message: string) {
        this.messageService.add('WeatherService: ' + message);
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
