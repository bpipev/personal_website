import { Injectable, NgZone } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { PlaygroundService } from './playground.service';
import { UserService, LOGIN_TYPE } from './user.service';

@Injectable({
    providedIn: 'root'
})
export class GoogleLoginService {
    private CLIENT_ID =
        '367118618715-ee8gk239gvh7vtpt6mgski3c11h956ih.apps.googleusercontent.com';
    private userChangedSource = new ReplaySubject<gapi.auth2.GoogleUser>(1);

    userChanged$ = this.userChangedSource.asObservable();
    constructor(
        private _ngZone: NgZone,
        private ps: PlaygroundService,
        private us: UserService
    ) {}

    private userChanged(user: gapi.auth2.GoogleUser) {
        if (user.isSignedIn()) {
            if (!this.us.isSignedIn) { // TODO: is this necessary??
                const user_id = user.getAuthResponse().id_token;
                this.ps.googleLogin(user_id).subscribe(response => {
                    this.us.userUpdated(response.token, LOGIN_TYPE.GOOGLE);
                });
            }
        } else {
            this.us.logout();
        }
        this.userChangedSource.next(user);
    }

    initSignIn() {
        gapi.load('auth2', () => {
            const auth2 = gapi.auth2.init({
                client_id: this.CLIENT_ID,
                scope: 'profile email'
            });
            auth2.currentUser.listen((user: gapi.auth2.GoogleUser) => {
                this._ngZone.run(() => {
                    this.userChanged(user);
                });
            });
        });
    }

    async signOut() {
        const auth2 = gapi.auth2.getAuthInstance();
        await auth2.signOut();
    }
}
