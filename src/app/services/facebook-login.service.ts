import { Injectable, NgZone } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { UserService, LOGIN_TYPE } from './user.service';
import { PlaygroundService } from './playground.service';
import { AppConfigService } from './../app-config.service';

@Injectable({
    providedIn: 'root'
})
export class FacebookLoginService {
    userChanged$ = new Subject<fb.StatusResponse>();
    hasInitialized$ = new ReplaySubject(1);

    private fbInitParams = {
        appId: AppConfigService.settings.fb_app_id,
        status: true,
        cookie: true,
        xfbml: true,
        version: 'v3.1'
    };
    constructor(
        private _ngZone: NgZone,
        private userService: UserService,
        private playgroundService: PlaygroundService
    ) {}

    initSignin() {
        (function(d, s, id) {
            let js = <HTMLScriptElement>d.getElementsByTagName(s)[0];
            const fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = <HTMLScriptElement>d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        })(document, 'script', 'facebook-jssdk');
        this.init();
    }

    onFbAsyncInit = () => {
        this._ngZone.run(() => {
            this.hasInitialized$.next(1);
        });
    }

    private init() {
        this.hasInitialized$.subscribe(() => {
            FB.init(this.fbInitParams);
            FB.getLoginStatus((response: fb.StatusResponse) => {
                console.log(response);
            });
            FB.Event.subscribe(
                'auth.statusChange',
                (response: fb.StatusResponse) => {
                    this._ngZone.run(() => {
                        console.log(response);
                        if (response.status === 'connected') {
                            this.playgroundService
                                .facebookLogin(
                                    response.authResponse.accessToken
                                )
                                .subscribe(x =>
                                    this.userService.userUpdated(
                                        x.token,
                                        LOGIN_TYPE.FACEBOOK
                                    )
                                );
                        }
                        this.userChanged$.next(response);
                    });
                }
            );
        });
    }

    parse() {
        // TODO: take element as argument?
        FB.XFBML.parse(
            document.getElementById('fb-login-button').parentElement
        );
    }

    signOut() {
        // TODO: loading
        this.hasInitialized$.subscribe(() => {
            FB.init(this.fbInitParams);
            FB.getLoginStatus((response: fb.StatusResponse) => {
                console.log(response);
                if (response.status === 'connected') {
                    FB.logout((resp: fb.StatusResponse) => {
                        console.log(resp);
                    });
                }
                this.userService.logout();
            });
        });
    }
}
