import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ThemeService {
    private key = 'theme';
    themeSource = new Subject<string>();
    theme$ = this.themeSource.asObservable();
    constructor() {}

    get theme(): string {
        return localStorage.getItem(this.key);
    }

    set theme(theme: string) {
        this.themeSource.next(theme);
        localStorage.setItem(this.key, theme);
    }

    init() {
        const theme = localStorage.getItem(this.key);
        this.themeSource.next(theme);
    }
}
