import { Injectable } from '@angular/core';
import { Person, DayScheduleType, Kvp, Schedule } from './../person';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfigService } from './../app-config.service';
import { MessageService } from './message.service';
import { catchError } from 'rxjs/operators';
import { WorkPreference } from './../WorkPreference';

@Injectable({
    providedIn: 'root'
})
export class ScheduleBackEndService {
    private domain: string;

    constructor(private http: HttpClient, private messageService: MessageService) {
        this.domain = AppConfigService.settings.apis.playground.domain;
    }

    removePersonFromSchedule(schedule_id: number, person_id: number) {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.scheduledPerson}`;
        const options = { params: { schedule_id: String(schedule_id), person_id: String(person_id) } };
        return this.http
            .delete(url, options)
            .pipe(catchError(this.handleError('removePersonFromSchedule')));
    }

    getPeopleNotInSchedule(schedule_id: number): Observable<any> {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.schedulesPeople}`;
        const options = { params: { schedule_id: String(schedule_id) } };
        return this.http
            .get(url, options)
            .pipe(catchError(this.handleError('getPeopleNotInSchedule')));
    }

    addPerson(person: Person): Observable<any> {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.schedulesPeople}`;
        return this.http
            .post(url, person)
            .pipe(catchError(this.handleError('addPerson')));
    }

    addWorkPreference(work_preference: WorkPreference): Observable<any> {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.schedulesPeople}/${work_preference.person_id}`;
        return this.http
            .post(url, work_preference)
            .pipe(catchError(this.handleError('addWorkPreference')));
    }

    deleteWorkPreference(preference_id: number): Observable<any> {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.workPreference}/${preference_id}`;
        return this.http
            .delete(url)
            .pipe(catchError(this.handleError('deleteWorkPreference')));
    }
    
    getPerson(person_id: number): Observable<any> {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.schedulesPeople}/${person_id}`;
        return this.http
            .get(url)
            .pipe(catchError(this.handleError('getPerson')));
    }

    addSchedule(start: number, duration: number, add_people_from_id: number): Observable<any> {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.schedules}`;
        return this.http
            .post(url, { start: start, duration: duration, add_people_from: add_people_from_id })
            .pipe(catchError(this.handleError('addSchedule')));
    }

    getSchedule(id: number): Observable<any> {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.schedules}/${id}`;
        return this.http.get(url).pipe(catchError(this.handleError('getSchedule')));
    }

    deleteSchedule(schedule_id: number) {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.schedules}/${schedule_id}`;
        return this.http.delete(url).pipe(catchError(this.handleError('deleteSchedule')));
    }

    updateDayValue(newValue: DayScheduleType, date: string, id: number, schedule_id: number): Observable<any> {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.schedules}`;
        const body = {
            new: newValue,
            date: date,
            id: id,
            schedule_id: schedule_id
        };
        return this.http.put(url, body).pipe(catchError(this.handleError('updateDayValue')));
    }

    deletePerson(id: number): Observable<any> {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.schedules}`;
        const httpParams = new HttpParams().set('id', String(id));
        const options = { params: httpParams };
        return this.http.delete(url, options).pipe(catchError(this.handleError('deletePerson')));
    }

    getScheduleList(): Observable<any> {
        const url = `${this.domain}${AppConfigService.settings.apis.playground.paths.schedules}`;
        return this.http.get(url).pipe(catchError(this.handleError('getScheduleList')));
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a PlaygroundService message with the messageService */
    private log(message: string) {
        this.messageService.add('PlaygroundService: ' + message);
    }
}
