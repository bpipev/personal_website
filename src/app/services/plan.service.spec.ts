import { TestBed } from '@angular/core/testing';

import { PlanService } from './plan.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfigService } from './app-config.service';

describe('PlanService', () => {
    beforeEach(() => {
        AppConfigService.settings = {
            env: {
                name: 'string'
            },
            apis: {
                playground: {
                    domain: 'string',
                    paths: {
                        login: 'string',
                        plans: 'string',
                        steps: 'string',
                        logout: 'string'
                    }
                }
            }
        };
        TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
    });

    it('should be created', () => {
        const service: PlanService = TestBed.get(PlanService);
        expect(service).toBeTruthy();
    });
});
