import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { User } from './../user';
import { Session } from './../session';

export enum LOGIN_TYPE {
    GOOGLE,
    FACEBOOK
}

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private userUpdatedSource = new ReplaySubject<User>(1); // TODO: pass username
    userUpdated$ = this.userUpdatedSource.asObservable();
    isSignedIn = false;
    loginType: LOGIN_TYPE;
    private sessionKey = 'myPlaygroundKey';

    constructor() {}

    initialize() {
        if (this.getSession() != null) {
            this.userUpdated(
                this.getSession().token,
                this.getSession().loginType
            );
        } else {
            this.userUpdatedSource.next({ isSignedIn: false });
        }
    }

    userUpdated(token: string, loginType: LOGIN_TYPE) {
        this.isSignedIn = token !== null;
        sessionStorage.setItem(
            this.sessionKey,
            JSON.stringify({ token: token, loginType: loginType } as Session)
        );
        this.loginType = loginType;
        this.userUpdatedSource.next({
            isSignedIn: this.isSignedIn
        });
    }

    sendSession() {
        if (this.getToken() !== null) {
            localStorage.setItem(
                'sessionStorage',
                JSON.stringify({
                    token: this.getToken(),
                    loginType: this.loginType
                } as Session)
            );
            localStorage.removeItem('sessionStorage');
        }
    }

    logout() {
        sessionStorage.removeItem(this.sessionKey);
        this.isSignedIn = false;
        this.loginType = null;
        this.userUpdatedSource.next({
            isSignedIn: false
        });
    }

    getToken() {
        return this.getSession() == null ? null : this.getSession().token;
    }

    getSession(): Session {
        let session;
        try {
            session = JSON.parse(
                sessionStorage.getItem(this.sessionKey)
            ) as Session;
        } catch {
            session = null;
        }
        return session;
    }
}
