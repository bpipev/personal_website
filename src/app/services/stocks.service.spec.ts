import { TestBed, inject } from '@angular/core/testing';

import { StocksService } from './stocks.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('StocksService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [StocksService],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        });
    });

    it('should be created', inject(
        [StocksService],
        (service: StocksService) => {
            expect(service).toBeTruthy();
        }
    ));
});
