import { TestBed, inject } from '@angular/core/testing';

import { PlaygroundService } from './playground.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfigService } from './app-config.service';

describe('PlaygroundService', () => {
    beforeEach(() => {
        AppConfigService.settings = {
            env: {
                name: 'string'
            },
            apis: {
                playground: {
                    domain: 'string',
                    paths: {
                        login: 'string',
                        plans: 'string',
                        steps: 'string',
                        logout: 'string'
                    }
                }
            }
        };
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [PlaygroundService]
        });
    });

    it('should be created', inject(
        [PlaygroundService],
        (service: PlaygroundService) => {
            expect(service).toBeTruthy();
        }
    ));
});
