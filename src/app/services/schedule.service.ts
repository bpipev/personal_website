import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Person, Schedule, DayScheduleType } from './../person';
import { MessageService } from './message.service';
import { ScheduleBackEndService } from './schedule-back-end.service';
import { tap } from 'rxjs/operators';
import { WorkPreference } from './../WorkPreference';

@Injectable({
    providedIn: 'root'
})
export class ScheduleService {
    private scheduleChangedSource = new Subject<Schedule>();
    private scheduleListSource = new Subject<any[]>();

    scheduleChanged$ = this.scheduleChangedSource.asObservable();
    scheduleList$ = this.scheduleListSource.asObservable();

    constructor(private messageService: MessageService, private scheduleBackEnd: ScheduleBackEndService) { }

    load(schedule_id: number) {
        this.log('Starting to load schedule');

        this.scheduleBackEnd.getSchedule(schedule_id).subscribe(schedule => {
            this.scheduleChangedSource.next(schedule);
        });
    }

    deleteSchedule(schedule_id: number) {
        return this.scheduleBackEnd.deleteSchedule(schedule_id);
    }

    addPerson(person: Person) {
        this.scheduleBackEnd.addPerson(person).subscribe(schedule => {
            this.scheduleChangedSource.next(schedule);
        });
    }

    getPerson(person_id: number) {
        return this.scheduleBackEnd.getPerson(person_id);
    }

    addWorkPreference(work_preference: WorkPreference) {
        return this.scheduleBackEnd.addWorkPreference(work_preference);
    }

    deleteWorkPreference(preference_id: number) {
        return this.scheduleBackEnd.deleteWorkPreference(preference_id);
    }

    getPeopleNotInSchedule(schedule_id: number) {
        return this.scheduleBackEnd.getPeopleNotInSchedule(schedule_id);
    }

    addSchedule(start: number, duration: number, add_people_from_id: number) {
        return this.scheduleBackEnd.addSchedule(start, duration, add_people_from_id);//.pipe(tap(schedule => { this.scheduleChangedSource.next(schedule); }));
    }

    updateDayValue(newValue: DayScheduleType, date: string, id: number, schedule_id: number): Observable<any> {
        return this.scheduleBackEnd.updateDayValue(newValue, date, id, schedule_id);
    }

    deletePerson(id: number): Observable<any> {
        return this.scheduleBackEnd.deletePerson(id);
    }

    removePersonFromSchedule(schedule_id: number, person_id: number): Observable<any> {
        return this.scheduleBackEnd.removePersonFromSchedule(schedule_id, person_id);
    }

    getScheduleList() {
        return this.scheduleBackEnd.getScheduleList().pipe(tap(x => {
            this.scheduleListSource.next(x);
        }));
    }

    private log(message: string) {
        this.messageService.add('ScheduleService: ' + message);
    }
}
