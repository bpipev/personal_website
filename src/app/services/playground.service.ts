import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfigService } from './../app-config.service';
import { Observable, of, from, forkJoin } from 'rxjs';
import { Plan } from './../plan';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { MessageService } from './message.service';
import { PlanStep } from './../plan-steps';
import { openDB } from 'idb';
import { Dashboard } from './../dashboard';
import { Status } from './../Status';
import { DateTime } from 'luxon';

@Injectable({
    providedIn: 'root'
})
export class PlaygroundService {
    private readonly LOCAL_KEYSTORE_NAME = 'PLAYGROUND';
    private readonly PLANS_OBJECT_STORE = 'PLANS';
    private readonly LOCAL_KEYSTORE_DB_VERSION = 2;
    private domain: string;
    private db = openDB(
        this.LOCAL_KEYSTORE_NAME,
        this.LOCAL_KEYSTORE_DB_VERSION,
        {
            upgrade: (db, oldVersion, newVersion, transaction) => {
                console.log('upgrade');
                db.createObjectStore(this.PLANS_OBJECT_STORE);
            },
            blocked: () => {
                this.log('Blocked');
            },
            blocking: () => {
                this.log('Blocking');
            },
            terminated: () => {
                this.log('Terminated');
            }
        }
    );

    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) {
        this.domain = AppConfigService.settings.apis.playground.domain;
    }

    googleLogin(id: string): Observable<any> {
        // TODO: response interface
        const auth = `Bearer ${id}`;
        const httpOptions = {
            headers: new HttpHeaders({ Authorization: auth })
        };
        const loginUrl = this.domain
            .concat(AppConfigService.settings.apis.playground.paths.login)
            .concat('/google');
        return this.http
            .get(loginUrl, httpOptions)
            .pipe(catchError(this.handleError('googleLogin')));
    }

    facebookLogin(id: string): Observable<any> {
        // TODO: response interface
        const httpOptions = {
            headers: new HttpHeaders({ Authorization: id })
        };
        const loginUrl = this.domain
            .concat(AppConfigService.settings.apis.playground.paths.login)
            .concat('/facebook');
        return this.http
            .get(loginUrl, httpOptions)
            .pipe(catchError(this.handleError('facebookLogin')));
    }

    getPlan(
        planId: number,
        isLocal: boolean,
        isSignedIn: boolean
    ): Observable<Plan> {
        if (!isSignedIn || isLocal) {
            return from(this.getLocalPlan(planId));
        } else {
            const getPlanUrl = `${this.domain}${
                AppConfigService.settings.apis.playground.paths.plans
            }/${planId}`;

            return this.http
                .get<Plan>(getPlanUrl)
                .pipe(catchError(this.handleError<Plan>('getPlan')));
        }
    }

    getDashboard(): Observable<Dashboard> {
        const getPlanUrl = `${this.domain}${
            AppConfigService.settings.apis.playground.paths.public
        }`;
        return this.http
            .get<Dashboard>(getPlanUrl)
            .pipe(catchError(this.handleError<Dashboard>('getDashboardPlans')));
    }

    getPublicPlan(planId: number): Observable<Plan> {
        const getPlanUrl = `${this.domain}${
            AppConfigService.settings.apis.playground.paths.publicPlan
        }/${planId}`;
        return this.http
            .get<Plan>(getPlanUrl)
            .pipe(catchError(this.handleError<Plan>('getPublicPlan')));
    }

    makePlanPublic(planId: number): Observable<Status> {
        const makePlanPublicUrl = `${this.domain}${
            AppConfigService.settings.apis.playground.paths.makePlanPublic
        }/${planId}`;
        return this.http
            .get<Status>(makePlanPublicUrl)
            .pipe(catchError(this.handleError<Status>('makePlanPublic')));
    }

    makePlanPrivate(planId: number): Observable<Status> {
        const makePlanPrivateUrl = `${this.domain}${
            AppConfigService.settings.apis.playground.paths.makePlanPrivate
        }/${planId}`;
        return this.http
            .get<Status>(makePlanPrivateUrl)
            .pipe(catchError(this.handleError<Status>('makePlanPrivate')));
    }

    getPlans(isSignedIn: boolean): Observable<Plan[]> {
        if (!isSignedIn) {
            return from(this.getLocalPlans()).pipe(
                catchError(this.handleError<Plan[]>('getPlans'))
            );
        } else {
            const getPlanUrl = `${this.domain}${
                AppConfigService.settings.apis.playground.paths.plans
            }/`;

            // TODO: catch inner observable errors
            // https://www.learnrxjs.io/operators/combination/forkjoin.html
            return forkJoin(
                this.http.get<Plan[]>(getPlanUrl),
                this.getLocalPlans()
            ).pipe(
                map(([remote, local]) => {
                    remote.forEach(plan => (plan.isLocal = false));
                    local.forEach(plan => (plan.isLocal = true));
                    return remote.concat(local);
                }),
                catchError(this.handleError<Plan[]>('getPlans'))
            );
        }
    }

    private async getLocalPlans(): Promise<Plan[]> {
        const db = await this.db;
        const tx = db.transaction(
            [this.PLANS_OBJECT_STORE],
            'readonly'
        );
        return tx.objectStore(this.PLANS_OBJECT_STORE).getAll();
    }

    private async getLocalPlan(localPlanId: number): Promise<Plan> {
        const db = await this.db;
        const tx = db.transaction(
            [this.PLANS_OBJECT_STORE],
            'readonly'
        );
        return tx.objectStore(this.PLANS_OBJECT_STORE).get(localPlanId);
    }

    insertPlan(plan: Plan, isSignedIn: boolean): Observable<Plan> {
        if (!isSignedIn) {
            return from(this.insertLocalPlan(plan)).pipe(
                catchError(this.handleError<Plan>('insertPlan'))
            );
        } else {
            const insertPlanUrl = `${this.domain}${
                AppConfigService.settings.apis.playground.paths.plans
            }/`;
            plan.isLocal = false;
            return this.http
                .post<Plan>(insertPlanUrl, plan)
                .pipe(catchError(this.handleError<Plan>('insertPlan')));
        }
    }

    private async insertLocalPlan(plan: Plan): Promise<Plan> {
        const db = await this.db;
        const tx = db.transaction([this.PLANS_OBJECT_STORE], 'readwrite');
        const now = DateTime.local().toMillis();
        plan.isLocal = true;
        plan.plan_id = now; // TODO: use UUID?
        tx.objectStore(this.PLANS_OBJECT_STORE).put(plan, now);
        await tx.done;
        return Plan.clone(plan);
    }

    private async updateLocalPlan(plan: Plan): Promise<Plan> {
        const db = await this.db;
        const tx = db.transaction(
            [this.PLANS_OBJECT_STORE],
            'readwrite'
        );
        plan.isLocal = true;
        tx.objectStore(this.PLANS_OBJECT_STORE).put(
            plan,
            plan.plan_id
        );
        await tx.done;
        return Plan.clone(plan);
    }

    insertStep(
        planStep: PlanStep,
        isLocal: boolean,
        isSignedIn: boolean
    ): Observable<PlanStep> {
        if (!isSignedIn || isLocal) {
            return from(this.getLocalPlan(planStep.plan_id)).pipe(
                mergeMap(plan => {
                    const now = DateTime.local().toMillis();
                    planStep.step_id = now;
                    plan.steps.push(planStep);
                    return from(this.updateLocalPlan(plan)).pipe(
                        map(x => {
                            return PlanStep.clone(planStep);
                        })
                    );
                })
            );
        } else {
            const insertStepUrl = `${this.domain}${
                AppConfigService.settings.apis.playground.paths.steps
            }/`;
            return this.http
                .post<PlanStep>(insertStepUrl, planStep)
                .pipe(catchError(this.handleError<PlanStep>('insertStep')));
        }
    }

    deletePlan(plan: Plan, isSignedIn: boolean): Observable<any> {
        if (!isSignedIn || plan.isLocal) {
            return from(this.deleteLocalPlan(plan)).pipe(
                catchError(this.handleError('deletePlan'))
            );
        } else {
            const getPlanUrl = `${this.domain}${
                AppConfigService.settings.apis.playground.paths.plans
            }/${plan.plan_id}`;
            return forkJoin(
                this.http.delete(getPlanUrl),
                this.deleteLocalPlan(plan)
            ).pipe(catchError(this.handleError('deletePlan')));
        }
    }

    private async deleteLocalPlan(plan: Plan): Promise<any> {
        const db = await this.db;
        const tx = db.transaction(
            [this.PLANS_OBJECT_STORE],
            'readwrite'
        );
        tx.objectStore(this.PLANS_OBJECT_STORE).delete(plan.plan_id);
        await tx.done;
    }

    deleteStep(
        planStep: PlanStep,
        isLocal: boolean,
        isSignedIn: boolean
    ): Observable<any> {
        if (!isSignedIn || isLocal) {
            return from(this.getLocalPlan(planStep.plan_id)).pipe(
                mergeMap(plan => {
                    const steps = plan.steps.filter(element => {
                        return element.step_id !== planStep.step_id;
                    });
                    plan.steps = steps;
                    return from(this.updateLocalPlan(plan)).pipe(
                        map(x => {
                            return PlanStep.clone(planStep);
                        })
                    );
                })
            );
        } else {
            const getStepUrl = `${this.domain}${
                AppConfigService.settings.apis.playground.paths.steps
            }/${planStep.step_id}`;

            return this.http
                .delete(getStepUrl)
                .pipe(catchError(this.handleError('deleteStep')));
        }
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a PlaygroundService message with the messageService */
    private log(message: string) {
        this.messageService.add('PlaygroundService: ' + message);
    }
}
