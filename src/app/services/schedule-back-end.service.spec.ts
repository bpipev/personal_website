import { TestBed } from '@angular/core/testing';

import { ScheduleBackEndService } from './schedule-back-end.service';

describe('ScheduleBackEndService', () => {
  let service: ScheduleBackEndService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScheduleBackEndService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
