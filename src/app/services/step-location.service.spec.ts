import { TestBed } from '@angular/core/testing';

import { StepLocationService } from './step-location.service';

describe('StepLocationService', () => {
  let service: StepLocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StepLocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
