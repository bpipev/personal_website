import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Plan } from './../plan';
import { Dashboard } from './../dashboard';
import { PlaygroundService } from './playground.service';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {
    private dashboardPlanChangedSource = new BehaviorSubject<Array<Plan>>(
        new Array<Plan>()
    );

    dashboardPlanChanged$ = this.dashboardPlanChangedSource.asObservable();
    constructor(private playgroundService: PlaygroundService) {}

    getDashboard(): Observable<Dashboard> {
        return this.playgroundService.getDashboard();
    }
}
