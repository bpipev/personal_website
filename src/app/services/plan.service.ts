import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject, Subject } from 'rxjs';
import { Plan } from './../plan';
import { PlaygroundService } from './playground.service';
import { tap } from 'rxjs/operators';
import { PlanStep } from './../plan-steps';
import { Status } from './../Status';

@Injectable({
    providedIn: 'root'
})
export class PlanService {
    // https://blog.angular-university.io/how-to-build-angular2-apps-using-rxjs-observable-data-services-pitfalls-to-avoid/
    private planChangedSource = new BehaviorSubject<Array<Plan>>(
        new Array<Plan>()
    );
    private isLoadingSource = new BehaviorSubject<Boolean>(false);
    private stepAddedSource = new Subject<PlanStep>();

    planChanged$ = this.planChangedSource.asObservable();
    isLoading$ = this.isLoadingSource.asObservable();
    hasAddedStep$ = this.stepAddedSource.asObservable();

    constructor(private playgroundService: PlaygroundService) {
        this.isLoadingSource.next(true);
    }

    addPlan(plan: Plan, isSignedIn: boolean): Observable<Plan> {
        return this.playgroundService.insertPlan(plan, isSignedIn).pipe(
            tap((x: Plan) => {
                this.planChangedSource.getValue().push(x);
                this.planChangedSource.next(this.planChangedSource.getValue());
            })
        );
    }

    addStep(
        planStep: PlanStep,
        isLocal: boolean,
        isSignedIn: boolean
    ): Observable<PlanStep> {
        return this.playgroundService
            .insertStep(planStep, isLocal, isSignedIn)
            .pipe(
                tap((addedStep: PlanStep) => {
                    this.stepAddedSource.next(addedStep);
                })
            );
    }

    deleteStep(
        planStep: PlanStep,
        isLocal: boolean,
        isSignedin: boolean
    ): Observable<any> {
        return this.playgroundService.deleteStep(planStep, isLocal, isSignedin);
    }

    getPlan(
        planId: number,
        isLocal: boolean,
        isSignedIn: boolean
    ): Observable<Plan> {
        return this.playgroundService.getPlan(planId, isLocal, isSignedIn);
    }

    getPlanList(isSignedIn: boolean) {
        this.planChangedSource.next(new Array<Plan>());
        this.isLoadingSource.next(true);
        this.playgroundService
            .getPlans(isSignedIn)
            .subscribe((plans: Plan[]) => {
                plans.forEach((element: Plan) => {
                    this.planChangedSource.getValue().push(element);
                });
                this.planChangedSource.next(this.planChangedSource.getValue());
                this.isLoadingSource.next(false);
            });
    }

    getPublicPlanList(planId: number): Observable<Plan> {
        return this.playgroundService.getPublicPlan(planId);
    }

    makePlanPublic(planId: number): Observable<Status> {
        return this.playgroundService.makePlanPublic(planId);
    }

    makePlanPrivate(planId: number): Observable<Status> {
        return this.playgroundService.makePlanPrivate(planId);
    }

    deletePlan(plan: Plan, isSignedIn: boolean): Observable<any> {
        return this.playgroundService.deletePlan(plan, isSignedIn).pipe(
            tap(() => {
                const index = this.planChangedSource.getValue().indexOf(plan);
                this.planChangedSource.getValue().splice(index, 1);
                this.planChangedSource.next(this.planChangedSource.getValue());
            })
        );
    }
}
