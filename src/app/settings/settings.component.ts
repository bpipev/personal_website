import { Component, Inject, OnInit } from '@angular/core';
import { ThemeService } from '../services/theme.service';
import { MatRadioChange } from '@angular/material/radio';
import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
    themes: any[] = [
        { name: 'Light theme', value: 'light' },
        { name: 'Dark theme', value: 'dark-theme' }
    ];
    languages: any[] = [
        { name: 'English', value: 'en-US' },
        { name: 'Français', value: 'fr-CA' }
    ]
    selectedTheme: string;
    selectedLanguage: string;
    constructor(private themeService: ThemeService, @Inject(DOCUMENT) private document: Document) { }

    ngOnInit() {
        this.selectedTheme = this.themeService.theme;
    }

    setTheme(event: MatRadioChange) {
        this.themeService.theme = event.value;
    }

    setLanguage(event: MatRadioChange) {
        // TODO: save language
        this.document.location.href = `${this.document.location.origin}/${event.value}`;
    }
}
