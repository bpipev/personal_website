import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FancyscrollComponent } from './fancyscroll.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('FancyscrollComponent', () => {
    let component: FancyscrollComponent;
    let fixture: ComponentFixture<FancyscrollComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [FancyscrollComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FancyscrollComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
