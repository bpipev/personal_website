import {
    Component,
    OnInit,
    ViewChild,
    ViewChildren,
    ElementRef,
    QueryList,
    OnDestroy,
    Renderer2
} from '@angular/core';
import { Image } from '../image';

@Component({
    selector: 'app-fancyscroll',
    templateUrl: './fancyscroll.component.html',
    styleUrls: ['./fancyscroll.component.css']
})
export class FancyscrollComponent implements OnInit, OnDestroy {
    @ViewChild('fancytextdisappears') fancytextdisappears: ElementRef;
    @ViewChildren('rotatingimage', { read: ElementRef })
    rotatingimages: QueryList<ElementRef>;

    images = [
        new Image(
            'assets/images/close-up-compass-gold-841286.jpg',
            'Photo by Valentin Antonucci from Pexels'
        ),
        new Image(
            'assets/images/architecture-blur-chain-link-fence-951409.jpg',
            'Photo by Travis Saylor from Pexels'
        ),
        new Image(
            'assets/images/alphabets-background-blackboard-953208.jpg',
            'Photo by rawpixel.com from Pexels'
        )
    ];
    rotationX = 0;
    translationX = 0;
    translationY = 0;
    translationZ = 0;
    limit = 500;
    listenerFunction: () => void;
    constructor(private renderer: Renderer2) { }

    ngOnInit() {
        this.listenerFunction = this.renderer.listen('window', 'scroll', (e) => {
            this.onScroll(e);
        });
    }

    private onScroll(e: Event) {
        this.hideFancyText();
        this.imageTransform();
        if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
            console.log('Bottom!');
        }
    }

    private imageTransform() {
        window.requestAnimationFrame(() => {
            this.rotatingimages.forEach((child, index) => {
                const viewportTopOffset = child.nativeElement.parentElement.getBoundingClientRect()
                    .top;
                this.rotationX = 0;
                if (viewportTopOffset < -child.nativeElement.clientHeight / 3) {
                    this.rotationX =
                        -(
                            -child.nativeElement.clientHeight / 3 -
                            viewportTopOffset
                        ) / 10;
                }
                child.nativeElement.style.transform = `rotateX(${
                    this.rotationX
                }deg)
            translate3d(${this.translationX}rem, ${this.translationY}rem, ${
                    this.translationZ
                }rem)`;
            });
        });
    }

    private hideFancyText() {
        const opacity = String(
            1 - document.documentElement.scrollTop / this.limit
        );
        (this.fancytextdisappears
            .nativeElement as HTMLHeadingElement).style.opacity = opacity;
    }

    ngOnDestroy(): void {
        if (this.listenerFunction) {
            this.listenerFunction();
        }
    }
}
