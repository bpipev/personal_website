export interface WorkPreference {
    id?: number;
    start: string;
    end: string;
    repeats: Repeats;
    is_work: boolean;
    person_id: number;
}

export enum Repeats {
    wekly = 'weekly',
    biweekly = 'biweekly',
    monthly = 'monthly'
}