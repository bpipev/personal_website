import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AnimationsComponent } from './animations/animations.component';
import { FancyscrollComponent } from './fancyscroll/fancyscroll.component';
import { PlanComponent } from './plan/plan.component';
import { PlanStepsComponent } from './plan-steps/plan-steps.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { SettingsComponent } from './settings/settings.component';
import { MapComponent } from './map/map.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { ScheduleListComponent } from './schedule/schedule-list/schedule-list.component';
import { ViewPersonComponent } from './schedule/view-person/view-person.component';
import { AiComponent } from './ai/ai.component';

const routes: Routes = [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'animations', component: AnimationsComponent },
    { path: 'plan', component: PlanComponent },
    { path: 'plan/:id', component: PlanStepsComponent },
    { path: 'plan/:id/map', component: MapComponent },
    { path: 'schedule', component: ScheduleListComponent },
    { path: 'schedule/:id', component: ScheduleComponent },
    { path: 'person/:id', component: ViewPersonComponent },
    { path: 'fancyscroll', component: FancyscrollComponent },
    { path: 'login', component: LoginComponent },
    { path: 'settings', component: SettingsComponent },
    { path: 'ai', component: AiComponent },
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { enableTracing: false, relativeLinkResolution: 'corrected' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
