import { PlanStep } from './plan-steps';

export class Plan {
    constructor(
        public title: string,
        public content: string,
        public steps = new Array<PlanStep>(),
        public plan_id?: number,
        public isLocal?: boolean,
        public is_public?: boolean
    ) {}
    static clone(plan: Plan) {
        return new Plan(
            plan.title,
            plan.content,
            plan.steps,
            plan.plan_id,
            plan.isLocal,
            plan.is_public
        );
    }
}
