import {
    Component,
    ViewChild,
    ElementRef,
    OnDestroy,
    AfterViewInit
} from '@angular/core';
import { StocksService } from '../services/stocks.service';

import { Chart, Legend, PointElement, LineElement, LineController, CategoryScale, LinearScale } from 'chart.js'
import { timer, Subject, forkJoin } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { GlobalQuoteResult } from '../stocks';
import { DateTime } from 'luxon';
import { MessageService } from '../services/message.service';

@Component({
    selector: 'app-stocks',
    templateUrl: './stocks.component.html',
    styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnDestroy, AfterViewInit {
    @ViewChild('myChart') myChartElement: ElementRef;
    currentStockSymbol: string;
    stockSymbols: { [symbol: string]: boolean } = { 'AAPL': true, 'GOOG': true, 'MSFT': true, 'SPY': true };
    batchStockSymbols: { [symbol: string]: string } = {
        'AAPL': '0',
        'GOOG': '0',
        'MSFT': '0'
    };
    private myChart: Chart;
    private onDestroy$ = new Subject();

    constructor(private stocksService: StocksService, private logger: MessageService) { }

    ngAfterViewInit() {
        Chart.register(Legend, LineElement, PointElement, LineController, CategoryScale, LinearScale);
        const ctx = this.myChartElement.nativeElement.getContext('2d');
        this.myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                plugins: {
                    legend: {}
                }
            }
        });

        this.myChart.options.plugins.legend.onClick = (e: any, legendItem: any) => {
            const index = legendItem.datasetIndex;
            this.myChart.data.datasets.forEach((element: any, idx: any) => {
                if (idx !== index) {
                    element.hidden = true;
                } else {
                    element.hidden = false;
                }
            });
            this.myChart.update();
        };

        this.getBatchStockQuotes(Object.keys(this.batchStockSymbols));
    }

    stockButtonClick(symbols: string) {
        this.getStocksTimeSeriesDaily(symbols);
    }

    getBatchStockQuotes(symbols: Array<string>) {
        const fiveMinutes = 5 * 60 * 1000;
        timer(0, fiveMinutes)
            .pipe(
                switchMap(() => {
                    return forkJoin(symbols.map(symbol => this.stocksService.getGlobalQuote(symbol)));
                }
                ),
                takeUntil(this.onDestroy$)
            )
            .subscribe(joined => {
                joined.forEach((value: GlobalQuoteResult) => {
                    if (value !== undefined) {
                        this.batchStockSymbols[value.symbol] = value.change_percent;
                    }
                });
            });
    }

    getStocksTimeSeriesDaily(symbols: string) {
        if (!symbols.trim()) {
            return;
        }
        // TODO: Pre-load one and save
        this.stocksService.getStocksTimeSeriesDaily(symbols).subscribe(x => {
            if (x === null) {
                this.log('API limit reached.');
            } else {
                this.stockSymbols[symbols] = false;
                this.addChartData(this.myChart, x.dataLabels, x.data1, x.title);
            }
        });
    }

    getStocksTimeSeriesIntraDay(symbols: string, interval: number) {
        this.stocksService
            .getStocksTimeSeriesIntraDay(symbols, interval)
            .subscribe(x => { });
    }

    addChartData(
        chart: Chart,
        timestamp: Array<Date>,
        data: Array<number>,
        label: string
    ) {
        timestamp = timestamp.slice(3, timestamp.length);
        data = data.slice(3, data.length);
        chart.data.datasets.forEach((element: any) => {
            element.hidden = true;
        });
        const dataSetIndex = chart.data.datasets.push({
            label: label,
            data: [],
            backgroundColor: ['rgba(255, 99, 132, 0.2)'],
            borderColor: ['rgba(255,99,132,1)'],
            borderWidth: 1,
            hidden: false,
            pointRadius: 1 // TODO: only hide when on phone
        });
        data.forEach((x, index) => {
            const time = DateTime.fromJSDate(timestamp[index]).toFormat('dd-MM-yyyy');
            chart.data.datasets[dataSetIndex - 1].data.push(Object.create({ x: time, y: x }));
        });
        chart.update();
    }

    ngOnDestroy(): void {
        this.onDestroy$.next(1);
    }

    private log(message: string) {
        this.logger.add('StocksComponent: ' + message);
    }
}
