import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import { serialization } from '@tensorflow/tfjs';
import { L1L2 } from '@tensorflow/tfjs-layers/dist/regularizers';
import { from } from 'rxjs';

class L2 extends L1L2 {
    getConfig(): serialization.ConfigDict {
        return super.getConfig();
    }

    static className = 'L2';

    constructor(config: any) {
        super(config);
    }

    static fromConfig<T extends serialization.Serializable>(
        cls: serialization.SerializableConstructor<T>,
        config: serialization.ConfigDict): T {
        return super.fromConfig(cls, config);
    }
}
@Component({
    selector: 'app-image',
    templateUrl: './image.component.html',
    styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
    @ViewChild('FileSelectInputDialog') FileSelectInputDialog: ElementRef;
    fileName: string;
    @ViewChild('canvas') canvas: ElementRef;
    context: CanvasRenderingContext2D;
    model: tf.LayersModel;
    prediction: string;
    constructor() { }

    ngOnInit(): void {
        tf.serialization.registerClass(L2);

        from(tf.loadLayersModel('assets/model/numbers/model.json')).subscribe(model => {
            this.model = model;
        });
    }
    ngAfterViewInit(): void {
        this.context = this.canvas.nativeElement.getContext("2d") as CanvasRenderingContext2D;
    }
    onFileSelected(event: Event): void {
        var URL = window.URL;
        var url = URL.createObjectURL((<HTMLInputElement>event.target).files[0]);
        const file: File = (<HTMLInputElement>event.target).files[0];
        if (file) {
            this.fileName = file.name;
            let baseImage = new Image();
            baseImage.src = url;
            baseImage.onload = () => {
                this.context.drawImage(baseImage, 0, 0, 28, 28);
                const imageData = this.context.getImageData(0, 0, 28, 28);
                let image = tf.browser.fromPixels(imageData, 1);
                image = image.div(255).reshape([1, 28, 28]); // get 0-1 values
                image = tf.cast(image, 'float32'); // cast to float32
                const prediction = this.model.predict(image);
                from((prediction as tf.Tensor<tf.Rank>).data()).subscribe(result => {
                    let probabilities = Array();
                    result.forEach((value: number, index: number) => {
                        probabilities.push(Object.create({number: index, probability: value}))
                    });
                    probabilities.sort((a, b) => {
                        return a.probability - b.probability;
                    }).reverse();
                    let probabilities_strings = Array<string>();
                    probabilities.forEach(element => {
                        probabilities_strings.push(`${element.number} : ${element.probability}`)
                    });
                    console.log(JSON.stringify(probabilities_strings));
                    let max = -1;
                    result.forEach((value: number, index: number) => {
                        max = Math.max(max, value);
                    });
                    if (max < 0) {
                        this.prediction = "Unknown"
                    } else {
                        this.prediction = String(result.indexOf(max));
                    }
                });
            }
        }
    }
}
