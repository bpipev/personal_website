//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import { initializers } from '@tensorflow/tfjs';

@Component({
    selector: 'app-nand',
    templateUrl: './nand.component.html',
    styleUrls: ['./nand.component.css']
})
export class NandComponent implements OnInit {

    constructor() { }

    ngOnInit(): void {
        const model = tf.sequential({
            layers: [
                tf.layers.dense({ inputShape: [784], units: 32, activation: 'relu' }),
                tf.layers.dense({ units: 10, activation: 'softmax' }),
            ]
        });
        model.weights.forEach(w => {
            console.log(w.name, w.shape);
        });

    }

}