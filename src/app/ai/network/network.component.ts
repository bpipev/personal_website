import { AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import { from } from 'rxjs';

@Component({
    selector: 'app-network',
    templateUrl: './network.component.html',
    styleUrls: ['./network.component.css']
})
export class NetworkComponent implements OnInit, AfterViewInit {
    @ViewChild('canvas') canvas: ElementRef;
    context: CanvasRenderingContext2D;
    clickStarted = false;
    model: tf.LayersModel;
    prediction: string;
    previous: any;
    constructor() {

    }

    clickStart(event: MouseEvent) {
        console.log(`click start x: ${event.offsetX}, y: ${event.offsetY}`);
        this.clickStarted = true;
    }

    @HostListener('mousemove', ['$event']) mouseMove(event: MouseEvent) {
        if (this.clickStarted) {
            this.draw(event.offsetX, event.offsetY);
        }
    }

    @HostListener('mouseup', ['$event']) mouseUp(event: MouseEvent) {
        this.clickStarted = false;
        this.previous = null;
    }

    draw(x: number, y: number) {
        if (this.previous) {
            this.context.beginPath();
            this.context.moveTo(this.previous[0], this.previous[1]);
            this.context.lineTo(x, y);
            this.context.stroke();
        }
        this.previous = [x, y];
    }

    ngOnInit(): void {
        from(tf.loadLayersModel('assets/model/numbers/model.json')).subscribe(model => {
            this.model = model;
        });
    }

    ngAfterViewInit() {
        this.context = this.canvas.nativeElement.getContext("2d") as CanvasRenderingContext2D;
        this.context.fillStyle = "black";
        this.context.fillRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
        this.context.fillStyle = "white";
        this.context.lineWidth = 25;
        this.context.lineCap = 'round';
        this.context.strokeStyle = '#FFFFFF';

    }

    predict() {
        this.context.drawImage(this.canvas.nativeElement, 0, 0, 28, 28);
        const imageData = this.context.getImageData(0, 0, 28, 28);
        let image = tf.browser.fromPixels(imageData, 1);
        image = image.div(255).reshape([1, 28, 28]); // get 0-1 values
        image = tf.cast(image, 'float32'); // cast to float32
        const prediction = this.model.predict(image);
        from((prediction as tf.Tensor<tf.Rank>).data()).subscribe(result => {
            let probabilities = Array();
            result.forEach((value: number, index: number) => {
                probabilities.push(Object.create({number: index, probability: value}))
            });
            probabilities.sort((a, b) => {
                return a.probability - b.probability;
            }).reverse();
            let probabilities_strings = Array<string>();
            probabilities.forEach(element => {
                probabilities_strings.push(`${element.number} : ${element.probability}`)
            });
            console.log(JSON.stringify(probabilities_strings));
            let max = -1;
            result.forEach((value: number, index: number) => {
                max = Math.max(max, value);
            });
            if (max < 0) {
                this.prediction = "Unknown"
            } else {
                this.prediction = String(result.indexOf(max));
            }
        });
    }
    clear() {
        this.context.clearRect(0, 0, 560, 560);
        this.context.fillStyle = "black";
        this.context.fillRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    }

}
