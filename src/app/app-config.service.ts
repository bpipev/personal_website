import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAppConfig } from '../app/app-config.model';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AppConfigService {
    static settings: IAppConfig;
    constructor(private http: HttpClient) {}

    load(): Promise<IAppConfig> {
        const env = environment.production ? 'prod' : 'dev';
        const jsonPath = `assets/config/${env}/config.json`;
        return this.http
            .get<IAppConfig>(jsonPath)
            .pipe(
                tap((x: IAppConfig) => {
                    AppConfigService.settings = x;
                })
            )
            .toPromise();
    }
}
