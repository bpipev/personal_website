import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PlanStepsComponent } from './plan-steps.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppConfigService } from '../app-config.service';

describe('PlanStepsComponent', () => {
    let component: PlanStepsComponent;
    let fixture: ComponentFixture<PlanStepsComponent>;

    beforeEach(waitForAsync(() => {
        AppConfigService.settings = {
            env: {
                name: 'string'
            },
            apis: {
                playground: {
                    domain: 'string',
                    paths: {
                        login: 'string',
                        plans: 'string',
                        steps: 'string',
                        logout: 'string'
                    }
                }
            }
        };
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterTestingModule],
            declarations: [PlanStepsComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PlanStepsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
