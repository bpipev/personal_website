import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';
import { StepLocationService } from 'src/app/services/step-location.service';

@Directive({
    selector: '[appStepValidation]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: StepValidationDirective,
            multi: true
        }
    ]
})
export class StepValidationDirective implements Validator {

    constructor(private stepLocationService: StepLocationService) { }
    validate(control: AbstractControl): ValidationErrors {
        console.log(control.value);
        if (control.dirty) {
            const needsText = control.value.needsText;
            const hasLocation = control.value.hasLocation;
            if (hasLocation) {
                const hasLocationLoaded = this.stepLocationService.hasLocationLoaded;
                if (!hasLocationLoaded) {
                    return { error: 'Need location value' };
                }
            }
            if (needsText) {
                const content = control.value.content;
                if (!content || content.length === 0) {
                    return { error: 'Need content value' };
                }
            }
            return null;
        } else {
            return { error: 'Need to enter values' };
        }
    }

}
