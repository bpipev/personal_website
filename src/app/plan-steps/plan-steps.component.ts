import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { PlanService } from '../services/plan.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Plan } from '../plan';
import { PlanStep } from '../plan-steps';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserService } from '../services/user.service';

@Component({
    selector: 'app-plan-steps',
    templateUrl: './plan-steps.component.html',
    styleUrls: ['./plan-steps.component.css']
})
export class PlanStepsComponent implements OnInit, OnDestroy {
    private onDestroy$ = new Subject();
    plan: Plan;
    isLoading: boolean;
    isPublic: boolean;
    isLocal: boolean;
    constructor(
        private planService: PlanService,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.isLoading = true;
        combineLatest([
            this.route.paramMap,
            this.route.queryParamMap,
            this.userService.userUpdated$
        ])
            .pipe(takeUntil(this.onDestroy$))
            .subscribe(([params, queryParams, user]) => {
                const planId = Number(params.get('id'));
                const isLocal = queryParams.get('isLocal') === 'true';
                this.isLocal = isLocal;
                this.isPublic = queryParams.get('isPublic') === 'true';
                if (this.isPublic) {
                    this.planService
                        .getPublicPlanList(planId)
                        .subscribe(plan => {
                            this.plan = plan;
                            this.isLoading = false;
                        });
                } else {
                    if (!user.isSignedIn && !isLocal) {
                        this.router.navigate(['/plan']);
                    } else {
                        this.planService
                            .getPlan(planId, isLocal, user.isSignedIn)
                            .subscribe(plan => {
                                this.plan = plan;
                                this.isLoading = false;
                            });
                    }
                }
            });
        this.planService.hasAddedStep$
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((addedStep: PlanStep) => {
                this.plan.steps.push(addedStep);
            });
    }

    goToPlansPage() {
        this.router.navigate(['/plan']);
    }

    goToMap() {
        this.router.navigate(['/plan', this.plan.plan_id, 'map'], {
            queryParams: {
                isLocal: [this.isLocal]
            }
        });
    }

    deleteStep(event: any) {
        const planStep = event.planStep;
        const index = event.index;
        this.planService
            .deleteStep(
                planStep,
                this.plan.isLocal,
                this.userService.isSignedIn
            )
            .subscribe(() => {
                this.plan.steps.splice(index, 1);
            });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next(1);
    }
}
