import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, NgZone } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PlanStep } from '../../plan-steps';
import { PlanService } from '../../services/plan.service';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, ReplaySubject, Subject, Subscription } from 'rxjs';
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';
import { StepLocationService } from 'src/app/services/step-location.service';

@Component({
    selector: 'app-add-step',
    templateUrl: './add-step.component.html',
    styleUrls: ['./add-step.component.css']
})
export class AddStepComponent implements OnInit, AfterViewInit {
    @Input() isPublic: boolean;
    @ViewChild('googleAutoComplete') googleAutoComplete: ElementRef;
    @ViewChild('planForm') planForm: NgForm;
    planstep = new PlanStep(-1, '', -1, null);
    isLocal: boolean;
    needsText = true;
    needsLocation = false;
    autocomplete: any;
    constructor(
        private planService: PlanService,
        private userService: UserService,
        private route: ActivatedRoute,
        private ngZone: NgZone,
        private messageService: MessageService,
        private stepLocationService: StepLocationService
    ) { }
    ngAfterViewInit(): void {
        if (this.googleAutoComplete) {
            this.autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(this.googleAutoComplete.nativeElement), {
                types: ['(cities)'],
                //   componentRestrictions: countryRestrict
            });
            this.autocomplete.addListener('place_changed', () => {
                this.ngZone.run(() => {
                    this.onPlaceChanged();
                });
            });
        }
    }

    private onPlaceChanged() {
        const place = this.autocomplete.getPlace();
        this.planstep.location = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng(), name: place.name };
        this.log(`lat: ${place.geometry.location.lat()}, lon: ${place.geometry.location.lng()}`);
        this.stepLocationService.hasLocationLoaded = true;
        this.planForm.control.updateValueAndValidity();
    }

    ngOnInit() {
        // https://stackoverflow.com/questions/42726437/observable-forkjoin-of-angular-route-params-observable
        // forkJoin emits the lastvalue from each observable, when all observables complete
        // this.route.paramMap will never complete
        combineLatest([this.route.paramMap, this.route.queryParamMap]).subscribe(
            ([params, queryParams]) => {
                this.planstep.plan_id = Number(params.get('id'));
                this.isLocal = queryParams.get('isLocal') === 'true';
            }
        );
    }

    onSubmit(planForm: NgForm) {
        if (this.needsLocation) {
            this.log(`addStep: ${JSON.stringify(this.planstep)}`)
            this.addStep(planForm);
            this.stepLocationService.hasLocationLoaded = false;
        } else {
            this.addStep(planForm);
        }
    }

    private addStep(planForm: NgForm) {
        this.planService
            .addStep(this.planstep, this.isLocal, this.userService.isSignedIn)
            .subscribe(() => {
                planForm.reset({hasText: true});
                this.planstep = new PlanStep(-1, '', this.planstep.plan_id, null);
                this.stepLocationService.hasLocationLoaded = false;
            });
    }

    private log(message: string) {
        this.messageService.add('CalendarView: ' + message);
    }
}
