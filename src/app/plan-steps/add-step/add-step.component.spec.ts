import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddStepComponent } from './add-step.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { PlanService } from '../../plan.service';
import { PlanStep } from '../../plan-steps';
import { Observable } from 'rxjs';

describe('AddStepComponent', () => {
    let component: AddStepComponent;
    let fixture: ComponentFixture<AddStepComponent>;
    let planServiceStub: Partial<PlanService>;

    planServiceStub = {
        addStep(
            planStep: PlanStep,
            isLocal: boolean,
            isSignedIn: boolean
        ): Observable<PlanStep> {
            return Observable.create();
        }
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [ReactiveFormsModule, FormsModule, RouterTestingModule],
            declarations: [AddStepComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [{ provide: PlanService, useValue: planServiceStub }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddStepComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
