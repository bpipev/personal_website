import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { PlanStep } from '../../plan-steps';

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.css']
})
export class StepComponent implements OnInit, AfterViewInit {
  @Input() step: PlanStep;
  @Input() index: number;
  @Input() isPublic: boolean;
  @Output() deleteStep = new EventEmitter();
  @ViewChild('gmap') gmapElement: ElementRef;
  private map: google.maps.Map;
  showMap: boolean;
  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.showMap = Boolean(this.step.location);
    if (this.showMap) {
      const mapPos = new google.maps.LatLng(
        this.step.location.lat,
        this.step.location.lng
      );
      const mapProp = {
        center: mapPos,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      this.map = new google.maps.Map(
        this.gmapElement.nativeElement,
        mapProp
      );
      const marker = new google.maps.Marker({
        position: mapPos,
        map: this.map
      });
    }
  }

  onDeleteStep() {
    this.deleteStep.emit({ planStep: this.step, index: this.index });
  }

}
