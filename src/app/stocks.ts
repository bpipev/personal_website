export class Stocks {
    title: string;
    labels = new Array<string>();
    data1 = new Array<number>();
    data2 = new Array<number>();
    data3 = new Array<number>();
    data4 = new Array<number>();
    data5 = new Array<number>();
    dataLabels = new Array<Date>();

    public static fromText(text: string, title?: string): Stocks {
        // TODO: use CSV library
        if(text.includes("Thank you for using Alpha Vantage! Our standard API call frequency is 5 calls per minute and 500 calls per day")) {
            return null;
        }
        const result = new Stocks();
        result.title = title;
        const data = text.trim().split('\n');
        const labels = data.shift().split(',');
        data.forEach(splitData => {
            const items = splitData.split(',');
            const date = items[0].split('-');
            result.dataLabels.unshift(
                new Date(Number(date[0]), Number(date[1]) - 1, Number(date[2]))
            );
            result.data1.unshift(Number(items[1]));
            result.data2.unshift(Number(items[2]));
            result.data3.unshift(Number(items[3]));
            result.data4.unshift(Number(items[4]));
            result.data5.unshift(Number(items[5]));
        });
        result.labels = labels;
        return result;
    }
}

export class GlobalQuoteResult {
    symbol: string;
    open: number;
    high: number;
    low: number;
    price: number;
    volume: number;
    latest_trading_day: string;
    previous_close: number;
    change: number;
    change_percent: string;
    public static fromJsonText(text: string): GlobalQuoteResult {
        const result = new GlobalQuoteResult();
        const json = JSON.parse(text);
        if ('Note' in json) {
            return undefined;
        } else {
            result.symbol = json['Global Quote']['01. symbol'];
            result.open = Number(json['Global Quote']['02. open']);
            result.high = Number(json['Global Quote']['03. high']);
            result.low = Number(json['Global Quote']['04. low']);
            result.price = Number(json['Global Quote']['05. price']);
            result.volume = Number(json['Global Quote']['06. volume']);
            result.latest_trading_day = json['Global Quote']['07. latest trading day'];
            result.previous_close = Number(json['Global Quote']['08. previous close']);
            result.change = Number(json['Global Quote']['09. change']);
            result.change_percent = json['Global Quote']['10. change percent'];
            return result;
        }
    }
}
