import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../services/message.service';
import { ScheduleService } from '../../services/schedule.service';
import { Person, ScheduleLength } from '../../person';
import { NgForm } from '@angular/forms';
import { concatMap } from 'rxjs/operators';

@Component({
    selector: 'app-add-person',
    templateUrl: './add-person.component.html',
    styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {
    scheduleTypes: { [symbol: string]: ScheduleLength } = {
        '8hrs': ScheduleLength.HOURS_8,
        '12hrs': ScheduleLength.HOURS_12,
    };
    people: { id: number, name: string }[];
    newPerson = <Person>{
        work_week_hours: 40
    };

    constructor(private messageService: MessageService, private scheduleService: ScheduleService) { }

    ngOnInit(): void {
        this.scheduleService.scheduleChanged$.pipe(concatMap(schedule => {
            this.newPerson.schedule_id = schedule.id;
            return this.scheduleService.getPeopleNotInSchedule(schedule.id);
        })).subscribe(x => {
            this.people = x;
            this.people.unshift({ id: undefined, name: undefined });
        });
    }

    submit(form: NgForm): void {
        if (!this.newPerson.name) {
            this.newPerson.name = this.people.find(x => x.id == this.newPerson.id).name;
        }
        this.scheduleService.addPerson(this.newPerson);
        form.reset();
        this.log(`submit name: ${this.newPerson.name} id: ${this.newPerson.id}`);
    }

    private log(message: string) {
        this.messageService.add('AddPerson: ' + message);
    }
}
