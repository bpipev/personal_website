import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ScheduleService } from '../../services/schedule.service';
import { MessageService } from '../../services/message.service';
import { WorkPreference, Repeats } from '../../WorkPreference';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-view-person',
    templateUrl: './view-person.component.html',
    styleUrls: ['./view-person.component.css']
})
export class ViewPersonComponent implements OnInit {
    name: string;
    existing_work_preferences: WorkPreference[] = [];
    work_preference: WorkPreference = { start: undefined, end: undefined, repeats: undefined, is_work: false, person_id: undefined };
    repeatsValues: Repeats[];

    constructor(private route: ActivatedRoute, private messageService: MessageService, private scheduleService: ScheduleService) { }

    ngOnInit(): void {
        this.repeatsValues = Object.values(Repeats);
        this.route.paramMap.pipe(switchMap(params => {
            const person_id = Number(params.get('id'));
            this.work_preference.person_id = person_id;
            this.log(String(person_id));
            return this.scheduleService.getPerson(person_id);
        })).subscribe(person => {
            this.name = person.name;
            this.existing_work_preferences = person.work_preferences;
            this.log(JSON.stringify(person));
        });
    }

    private log(message: string) {
        this.messageService.add('ViewPerson: ' + message);
    }

    submit(form: NgForm): void {
        this.scheduleService.addWorkPreference(this.work_preference).subscribe(x => {
            this.log(`Submit ${JSON.stringify(this.work_preference)}`);
            this.existing_work_preferences.push(Object.assign({}, this.work_preference));
            form.reset();
        });
    }

    deleteWorkPreference(preference_id: number) {
        this.scheduleService.deleteWorkPreference(preference_id).subscribe(x => {
            const id = this.existing_work_preferences.findIndex(x => x.id === preference_id);
            this.existing_work_preferences.splice(id, 1);
            this.log(`deleted row ${id}`);
        });
    }
}
