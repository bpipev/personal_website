import { Component, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ScheduleService } from '../../services/schedule.service';
import { DayScheduleType } from '../../person';
import { DateTime, Interval } from 'luxon';
import { Subject } from 'rxjs';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MessageService } from '../../services/message.service';
import { MatSelect, MatSelectChange } from '@angular/material/select';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { RemovePersonComponent, RemoveType } from '../remove-person/remove-person.component';
import { WorkPreference } from '../../WorkPreference';
import { switchMap, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-calendar-view',
    templateUrl: './calendar-view.component.html',
    styleUrls: ['./calendar-view.component.css']
})
export class CalendarViewComponent implements OnInit, OnDestroy {
    @ViewChild(MatSort) set contentSort(sort: MatSort) {
        this.dataSource.sort = sort;
    }
    @ViewChild(MatTable) table: MatTable<any>;
    numberOfPeople: number;
    dataSource = new MatTableDataSource<any>();
    summary: any = { name: 'Summary' };
    displayedColumns = new Array<string>();
    options = [null, 'D', 'E', 'N', 'J', 'Z', '/'];
    option: string;
    isLoading = true;
    hasSchedules = false;
    private onDestroy$ = new Subject();

    constructor(private route: ActivatedRoute, private messageService: MessageService, private scheduleService: ScheduleService,
        public dialog: MatDialog, private router: Router, private renderer: Renderer2) { }

    ngOnDestroy(): void {
        this.onDestroy$.next(1);
    }

    ngOnInit(): void {
        // Use switchMap instead of mergeMap so that there's only one inner subscription
        this.route.paramMap.pipe(switchMap(params => {
            const schedule_id = Number(params.get('id'));
            this.scheduleService.load(schedule_id);
            return this.scheduleService.scheduleChanged$;
        }),
            // takeUntil should be the last operator https://cartant.medium.com/rxjs-avoiding-takeuntil-leaks-fb5182d047ef
            takeUntil(this.onDestroy$)).subscribe(schedule => {
                if (schedule) {
                    this.numberOfPeople = Object.keys(schedule.data).length;
                    this.displayedColumns = [];
                    this.displayedColumns.push('name');
                    this.displayedColumns = this.displayedColumns.concat(schedule.dates.sort().map(date => (DateTime.fromSeconds(date).toFormat('dd-MMM-yy'))));
                    this.summary = { name: 'Summary' };
                    const tableData = Object.keys(schedule.data).map(id => {
                        const data: any = {};
                        data.name = schedule.data[id].name;
                        data.id = id;
                        data.schedule_id = schedule.id;
                        Object.keys(schedule.data[id].workdays).forEach((key: any) => {
                            const date = DateTime.fromSeconds(Number(key));
                            const formattedDate = date.toFormat('dd-MMM-yy');
                            const dayScheduleType = schedule.data[id].workdays[Number(key)] as DayScheduleType;
                            if (!this.summary[formattedDate]) {
                                this.summary[formattedDate] = { d: 0, e: 0, n: 0, e1: 0, e2: 0 };
                            }
                            this.updateSummaryColumn(dayScheduleType, formattedDate);
                            data[formattedDate] = { type: dayScheduleType, preference: null };
                        });
                        schedule.dates.forEach(unix => {
                            const date = DateTime.fromSeconds(unix);
                            const formattedDate = date.toFormat('dd-MMM-yy');
                            let pref = <"work" | "vacation">null;
                            schedule.data[id].work_preferences.forEach((preference: WorkPreference) => {
                                const start = DateTime.fromFormat(preference.start, 'yyyy-MM-dd');
                                const end = DateTime.fromFormat(preference.end, 'yyyy-MM-dd');
                                const interval = Interval.fromDateTimes(start, end);
                                // TODO: handle repeats
                                if (interval.contains(date) || start.startOf('day').equals(date.startOf('day')) || end.startOf('day').equals(date.startOf('day'))) {
                                    pref = preference.is_work ? "work" : "vacation";
                                }
                            });
                            data[formattedDate] = { type: data[formattedDate]?.type, preference: pref };
                        });
                        return data;
                    });
                    this.log('Updating Data');
                    this.hasSchedules = true;
                    this.dataSource.data = tableData;
                }
                this.isLoading = false;
            });
    }

    viewPerson(row: any) {
        this.router.navigate(['/person', row.id]);
    }

    private updateSummaryColumn(dayScheduleType: DayScheduleType, formattedDate: string) {
        switch (dayScheduleType) {
            case DayScheduleType.D:
                this.summary[formattedDate].d += 1;
                break;
            case DayScheduleType.E:
                this.summary[formattedDate].e += 1;
                break;
            case DayScheduleType.N:
                this.summary[formattedDate].n += 1;
                break;
            case DayScheduleType.J:
                this.summary[formattedDate].d += 1;
                this.summary[formattedDate].e1 += 1;
                break;
            case DayScheduleType.Z:
                this.summary[formattedDate].n += 1;
                this.summary[formattedDate].e2 += 1;
                break;
            default:
                break;
        }
        while (this.summary[formattedDate].e1 > 0 && this.summary[formattedDate].e2 > 0) {
            this.summary[formattedDate].e += 1;
            this.summary[formattedDate].e1--;
            this.summary[formattedDate].e2--;
        }
    }

    updateCell(event: MatSelectChange, column: string, person: any) {
        const newValue = <DayScheduleType>event.value;
        const person_id = person.id;
        const date = DateTime.fromFormat(column, 'dd-MMM-yy').toFormat('yyyy-LL-dd');
        const schedule_id = person.schedule_id;
        this.scheduleService.updateDayValue(newValue, date, person_id, schedule_id).subscribe(x => {
            // TODO: handle errors
            if (newValue === DayScheduleType.off) {
                const work_preference: WorkPreference = { start: date, end: date, repeats: undefined, is_work: false, person_id: person_id };
                this.scheduleService.addWorkPreference(work_preference).subscribe(y => {
                    person[column].preference = 'vacation';
                });
            }
            this.summary = { name: 'Summary' };
            let old_value = this.dataSource.data.find(row => row.id === person_id)[column];
            this.dataSource.data.find(row => row.id === person_id)[column] = { type: newValue, preference: old_value?.preference };
            this.dataSource.data.forEach((dataRow: any) => {
                this.updateSummary(dataRow);
            });
            this.log(newValue);
        });
    }

    getDayType(date: string): string {
        const dateTime = DateTime.fromFormat(date, 'dd-LLL-yy');
        return dateTime.weekday === 6 || dateTime.weekday === 7 ? 'weekend' : 'weekday';
    }

    private updateSummary(dataRow: any) {
        Object.keys(dataRow).forEach(key => {
            if (dataRow[key].type) {
                if (!this.summary[key]) {
                    this.summary[key] = { d: 0, e: 0, n: 0, e1: 0, e2: 0 };
                }
                this.updateSummaryColumn(dataRow[key].type, key);
            }
        });
    }

    deleteRow(row: any) {
        const person_id = row.id;
        const schedule_id = row.schedule_id;
        this.summary = { name: 'Summary' };
        const dialog = this.dialog.open(RemovePersonComponent);
        dialog.afterClosed().subscribe(removeType => {
            switch (removeType) {
                case RemoveType.Permanent:
                    this.scheduleService.deletePerson(person_id).subscribe(x => {
                        // TODO: use observable data?
                        this.removeRowFromTable(person_id);
                        this.log(`name: ${row.name}, id: ${x.id} deleted`);
                    });
                    break;
                case RemoveType.Schedule:
                    this.scheduleService.removePersonFromSchedule(schedule_id, person_id).subscribe(x => {
                        if (x?.deleted) {
                            this.removeRowFromTable(person_id);
                        }
                        this.log(`name: ${row.name}, id: ${x.id} removed from schedule`);
                    });
                    break;
                case RemoveType.None:
                default:
                    this.log("None");
                    break;
            }
        });
    }

    private removeRowFromTable(person_id: number) {
        this.dataSource.data = this.dataSource.data.filter((dataRow: any, i) => {
            const exists = dataRow.id !== person_id;
            if (exists) {
                this.updateSummary(dataRow);
            }
            return exists;
        });
        this.table.renderRows();
    }

    private log(message: string) {
        this.messageService.add('CalendarView: ' + message);
    }

}
