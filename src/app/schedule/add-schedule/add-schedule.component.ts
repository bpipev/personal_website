import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageService } from '../../services/message.service';
import { ScheduleService } from '../../services/schedule.service';
import { DateTime } from 'luxon';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    selector: 'app-add-schedule',
    templateUrl: './add-schedule.component.html',
    styleUrls: ['./add-schedule.component.css']
})
export class AddScheduleComponent implements OnInit, OnDestroy {
    date: string;
    duration: number;
    addFromScheduleId: number;
    scheduleList: any[];
    private onDestroy$ = new Subject();
    
    constructor(private messageService: MessageService, private scheduleService: ScheduleService, private router: Router) { }
    ngOnDestroy(): void {
        this.onDestroy$.next(1);
    }

    ngOnInit(): void {
        this.duration = 14;
        this.scheduleService.scheduleList$.pipe(takeUntil(this.onDestroy$)).subscribe((list: []) => {
            this.scheduleList = list.map((x: any) => {
                const start = DateTime.fromSeconds(x.start).toFormat('dd-MMM-yy');
                return {name: `${x.department_name}: Start: ${start}`, id: x.schedule_id};
            });
            this.scheduleList.unshift({name: null, id: null});
        });
    }

    addSchedule() {
        const start = DateTime.fromFormat(this.date, 'yyyy-LL-dd').toSeconds();
        this.scheduleService.addSchedule(start, this.duration, this.addFromScheduleId).subscribe((currentSchedule: any) => {
            this.scheduleService.getScheduleList().subscribe(_ => {
                this.router.navigate(['/schedule', currentSchedule.id]);
            });
            this.log(`Adding new schedule Date: ${this.date}, Duration: ${this.duration}`);
        });
    }

    private log(message: string) {
        this.messageService.add('AddSchedule: ' + message);
    }
}
