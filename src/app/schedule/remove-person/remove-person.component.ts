import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

export enum RemoveType {
    Permanent,
    Schedule,
    None
}

@Component({
    selector: 'app-remove-person',
    templateUrl: './remove-person.component.html',
    styleUrls: ['./remove-person.component.css']
})
export class RemovePersonComponent implements OnInit {

    public get RemoveType(): typeof RemoveType {
        return RemoveType;
    }

    constructor(public dialogRef: MatDialogRef<RemovePersonComponent>) { }

    ngOnInit(): void {
    }

    remove(type: RemoveType) {
        this.dialogRef.close(type);
    }
}
