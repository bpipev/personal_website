import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageService } from '../../services/message.service';
import { ScheduleService } from '../../services/schedule.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DateTime } from 'luxon';

@Component({
    selector: 'app-schedule-list',
    templateUrl: './schedule-list.component.html',
    styleUrls: ['./schedule-list.component.css']
})
export class ScheduleListComponent implements OnInit, OnDestroy {
    onDestroy$ = new Subject();
    scheduleList: { department_name: string, start: string, schedule_id: number }[];
    uriPath: string;

    constructor(
        private messageService: MessageService,
        private scheduleService: ScheduleService,
        private router: Router) { }

    ngOnInit(): void {
        this.uriPath = `${document.location.origin}/schedule`;
        this.scheduleService.getScheduleList().subscribe();
        this.scheduleService.scheduleList$.pipe(takeUntil(this.onDestroy$)).subscribe((list: any[]) => {
            this.scheduleList = list.map((x: any) => {
                const start = DateTime.fromSeconds(x.start).toFormat('dd-MMM-yy');
                return { department_name: x.department_name, start: start, schedule_id: x.schedule_id };
            });
            this.log(`ScheduleList updated`);
        });
    }
    deleteSchedule(schedule_id: number) {
        this.scheduleService.deleteSchedule(schedule_id).subscribe((x: any) => {
            this.log(JSON.stringify(x));
            if (x?.deleted) {
                this.scheduleList = this.scheduleList.filter(x => x.schedule_id !== schedule_id);
            }
        });
    }

    private log(message: string) {
        this.messageService.add('ScheduleList: ' + message);
    }
    openSchedule(schedule_id: number) {
        this.router.navigate(['/schedule', schedule_id]);
    }
    ngOnDestroy() {
        this.onDestroy$.next(1);
    }
}
