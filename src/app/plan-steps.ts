export class PlanStep {
    constructor(
        public step_id: number,
        public content: string,
        public plan_id: number,
        public location?: {lat: number, lng: number, name: string}
    ) {}
    static clone(planStep: PlanStep): PlanStep {
        return new PlanStep(
            planStep.step_id,
            planStep.content,
            planStep.plan_id,
            planStep.location
        );
    }
}
