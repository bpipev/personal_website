export interface Person {
    name: string;
    schedule_id: number;
    schedule_type: ScheduleLength;
    work_week_hours: number;
    id: number;
}

export interface Schedule {
    id: number;
    dates: Array<number>;
    data: any;
    currentUser: number;
}

export interface Kvp {
    [key: number]: DayScheduleType;
}

export interface KvpString {
    [key: string]: string;
}

export enum DayScheduleType {
    D = 'D',
    E = 'E',
    N = 'N',
    J = 'J',
    Z = 'Z',
    off = '/'
}

export enum ScheduleLength {
    HOURS_8 = 8,
    HOURS_12 = 12
}
