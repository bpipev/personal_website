import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { Observable ,  Subject } from 'rxjs';
import { Weather } from '../weather';

@Component({
    selector: 'app-weather',
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
    @ViewChild('gmap') gmapElement: ElementRef;
    private map: google.maps.Map;

    currentWeather$: Observable<Weather>;
    weatherLocation: string;
    isMapVisible = false;
    weatherLocationEntry = new Set<string>();

    constructor(private weatherService: WeatherService) {}

    ngOnInit() {}

    getWeather(city: string): void {
        const cityName = city.trim();
        if (!cityName) {
            return;
        }
        this.isMapVisible = false;
        this.weatherLocation = cityName;
        this.currentWeather$ = this.weatherService.getWeatherByCity(cityName);
        this.weatherLocationEntry.delete('weather-location-entry');
    }

    findLocation(): void {
        if (!navigator.geolocation) {
            return;
        }
        navigator.geolocation.getCurrentPosition((position: GeolocationPosition) => {
            this.weatherLocation = 'Current Location';
            this.weatherLocationEntry.add('weather-location-entry');
            this.isMapVisible = true;
            this.currentWeather$ = this.weatherService.getWeatherByLocation(
                position
            );
            const mapPos = new google.maps.LatLng(
                position.coords.latitude,
                position.coords.longitude
            );
            const mapProp = {
                center: mapPos,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            this.map = new google.maps.Map(
                this.gmapElement.nativeElement,
                mapProp
            );
            const marker = new google.maps.Marker({
                position: mapPos,
                map: this.map
            });
        });
    }
}
