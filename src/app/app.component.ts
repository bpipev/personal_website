import { Component, HostListener, OnInit, Renderer2 } from '@angular/core';
import { environment } from '../environments/environment';
import { ThemeService } from './services/theme.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'my playground';
    environment = environment;
    deferredPrompt: BeforeInstallPromptEvent;
    shouldShowAddToHomeButton: boolean;

    constructor(private themeService: ThemeService, private renderer: Renderer2) { }

    ngOnInit(): void {
        this.themeService.theme$.subscribe(
            themeClass => (this.renderer.setAttribute(document.documentElement, 'class', themeClass))
        );
        this.themeService.init();
        // Finally, if your app's content is not placed inside of a mat-sidenav-container element,
        // you need to add the mat-app-background class to your wrapper element (for example the body).
        // This ensures that the proper theme background is applied to your page.
        // https://material.angular.io/guide/theming
        this.renderer.addClass(document.body, 'mat-app-background');
    }

    @HostListener('window:beforeinstallprompt', ['$event'])
    onBeforeInstallPrompt(e: BeforeInstallPromptEvent) {
        // https://developers.google.com/web/fundamentals/app-install-banners/#trigger-m68
        // https://stackoverflow.com/questions/51503754/typescript-type-beforeinstallpromptevent
        // Prevent Chrome 67 and earlier from automatically showing the prompt
        e.preventDefault();
        // Stash the event so it can be triggered later.
        this.deferredPrompt = e;

        console.log(`'onBeforeInstallPrompt' ${e}`);
        this.shouldShowAddToHomeButton = true;
    }

    @HostListener('window:appinstalled', ['$event'])
    onAppInstalled(e: Event) {
        console.log(`window:onAppInstalled ${e}`);
    }

    async addToHome() {
        if (this.deferredPrompt != null) {
            this.deferredPrompt.prompt();
            const result = await this.deferredPrompt.userChoice;
            if (result.outcome === 'accepted') {
                console.log('accepted');
            } else {
                console.log('dismissed');
            }
            this.deferredPrompt = null;
        }
    }
}
