import { Component, OnInit, ElementRef, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { combineLatest, Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { PlaygroundService } from '../services/playground.service';
import { Plan } from '../plan';
import { PlanStep } from '../plan-steps';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, AfterViewInit, OnDestroy {
    private onDestroy$ = new Subject();
    @ViewChild('gmap') gmapElement: ElementRef;
    isLoading: boolean;
    isPublic: boolean;
    isLocal: boolean;
    planId: number;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private playgroundService: PlaygroundService,
        private userService: UserService
    ) { }

    ngAfterViewInit(): void {
        this.isLoading = true;
        combineLatest([
            this.route.paramMap,
            this.route.queryParamMap,
            this.userService.userUpdated$
        ]).pipe(switchMap(([params, queryParams, user]) => {
            this.planId = Number(params.get('id'));
            this.isLocal = queryParams.get('isLocal') === 'true';
            // TODO: public plans
            return this.playgroundService.getPlan(this.planId, this.isLocal, user.isSignedIn);
        }), takeUntil(this.onDestroy$))
            .subscribe((plan: Plan) => {
                const steps: PlanStep[] = plan.steps.filter(x => x.location);
                const bounds = new google.maps.LatLngBounds();
                let mapProp = {
                    center: { lat: 0, lng: 0 },
                    zoom: 1,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                if (steps && steps.length > 0) {
                    mapProp = {
                        center: steps[0].location,
                        zoom: 10,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                }

                const map = new google.maps.Map(
                    this.gmapElement.nativeElement,
                    mapProp
                );
                steps.forEach((item) => {
                    const infoWindow = new google.maps.InfoWindow();
                    infoWindow.setContent(`<b style="color:black">${item.location.name}</b>`);
                    const mapPos = new google.maps.LatLng(
                        item.location.lat,
                        item.location.lng
                    );
                    const marker = new google.maps.Marker({
                        position: mapPos,
                        map: map
                    });
                    bounds.extend(marker.getPosition());
                    google.maps.event.addListener(marker, 'click', () => {
                        infoWindow.open(map, marker);
                    });
                });
                // Center map around markers
                map.fitBounds(bounds);
            });
    }

    goBack() {
        this.router.navigate(['/plan', this.planId], {
            queryParams: {
                isLocal: [this.isLocal]
            }
        });
    }

    ngOnDestroy(): void {
        this.onDestroy$.next(1);
    }

    ngOnInit(): void {
    }

}
