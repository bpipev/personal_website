import { LOGIN_TYPE } from './services/user.service';

export interface Session {
    token: string;
    loginType: LOGIN_TYPE;
}
