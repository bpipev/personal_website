import * as express from 'express';
import * as logger from 'morgan';
import * as path from 'path';
import * as https from 'https';
import * as http from 'http';
import * as compression from 'compression';
import * as fs from 'fs';
import { Response, Request, NextFunction } from 'express';
const hsts = require('hsts');
const isDevEnv = process.env.NODE_ENV === 'development'

let credentials = {};
if (isDevEnv) {
    const privateKey = fs.readFileSync('ssl/localhost.key');
    const certificate = fs.readFileSync('ssl/localhost.crt');
    credentials = { key: privateKey, cert: certificate };
}


const port = process.env.PORT || 3000;

const app = express();

app.use(logger('dev')); // TODO: prod?
app.use(compression());
app.use(hsts());
// https://stackoverflow.com/questions/8605720/how-to-force-ssl-https-in-express-js
app.use((req: Request, res: Response, next: NextFunction) => {
    // The 'x-forwarded-proto' check is for Heroku
    if (
        !req.secure &&
        req.get('x-forwarded-proto') !== 'https'
    ) {
        return res.redirect('https://' + req.get('host') + req.url);
    }
    next();
});
app.use(express.static(path.join(__dirname)));

app.get(['/fr-CA', '/fr-CA*'], (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, 'fr-CA', 'index.html'));
});
app.get('*', (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, 'en-US', 'index.html'));
});

app.set('port', port);

let server = isDevEnv ? https.createServer(credentials, app) : http.createServer(app);

server.listen(port, () => console.log(`Running`));
